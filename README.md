# Introduction
A toy example of using an RDF Store to store information.


# Running
## Setup
```
bash setup.sh
```


# References
- https://www.linkedin.com/pulse/importing-data-google-spreadsheet-using-sparql-kingsley-uyi-idehen/
- https://en.wikipedia.org/wiki/RDF_Schema
- https://en.wikipedia.org/wiki/Resource_Description_Framework#Vocabulary
- https://rdflib.readthedocs.io/en/stable/gettingstarted.html
